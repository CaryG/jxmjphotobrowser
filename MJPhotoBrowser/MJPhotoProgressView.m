//
//  MJPhotoProgressView.m
//
//  Created by mj on 13-3-4.
//  Copyright (c) 2013年 itcast. All rights reserved.
//

#import "MJPhotoProgressView.h"

#define kDegreeToRadian(x) (M_PI/180.0 * (x))

@implementation MJPhotoProgressView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        self.userInteractionEnabled = NO;
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGFloat centerX=0.5*self.bounds.size.width;
    CGFloat centerY=0.5*self.bounds.size.height;
    CGFloat lineWidth=1.5;
    CGFloat radius = 0.5* MIN(rect.size.height, rect.size.width)-0.5*lineWidth;
    UIBezierPath *path=[UIBezierPath bezierPath];
    CGFloat startAngle=-M_PI_2;
    CGFloat angle=2*M_PI*self.progress -M_PI_2;
    path.lineWidth=lineWidth;
    path.lineCapStyle=kCGLineCapRound;
    [path addArcWithCenter:CGPointMake(centerX, centerY) radius:radius startAngle:startAngle endAngle:angle clockwise:YES];
    [self.progressTintColor setStroke];
    [path stroke];

}

#pragma mark - Property Methods

- (UIColor *)trackTintColor
{
    if (!_trackTintColor)
    {
        _trackTintColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.7f];
    }
    return _trackTintColor;
}

- (UIColor *)progressTintColor
{
    if (!_progressTintColor)
    {
        _progressTintColor = [UIColor whiteColor];
    }
    return _progressTintColor;
}

- (void)setProgress:(float)progress
{
    _progress = progress;
    [self setNeedsDisplay];
}

@end
