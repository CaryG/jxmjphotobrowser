Pod::Spec.new do |s|

  s.name         = "JXMJPhotoBrowser"
  s.module_name  = 'MJPhotoBrowser'
  s.version      = "0.1.0"
  s.summary      = "The easiest lightest way to use PhotoBrowser, enhanced by Sunnyyoung."
  s.homepage     = "https://bitbucket.org/CaryG"
  s.license      = "MIT"

  s.authors      = { 'Cary' => 'guojiashuang@live.com' }

  s.platform     = :ios, "8.0"
  s.source       = { :git => "https://bitbucket.org/CaryG/jxmjphotobrowser", :tag => s.version }
  
  s.source_files = "MJPhotoBrowser/*.{h,m}"
  s.resource     = "MJPhotoBrowser/*.bundle"
  s.requires_arc = true
  
  s.dependency 'SDWebImage'
#   s.dependency 'SVProgressHUD'
#   s.dependency 'YLGIFImage'

end
